package de.vorb.gitlabci;

import com.google.common.truth.Truth;
import org.junit.Test;

public class HelloWorldTest {

    @Test
    public void testName() throws Exception {
        final String message = new HelloWorld().getMessage();
        Truth.assertThat(message).contains("Hello");
        Truth.assertThat(message).contains("World");
    }

}
